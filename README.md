# HTML5 Boilerplate - Webpack & Boostrap edition

A work in progress, constantly evolving HTML 5 boilerplate using Webpack and Bootstrap.

Crucially, this is using native bootsrap Javascript, because..JQuery...who needs it nowadays?

To get going, run
```node
npm install
```

For development, run  
```node
npm run build
```

Once you're ready for production, run
```node
npm run build:production
```

## To-do
- Tidy up dependencies

## Roadmap
- Use react
- TypeScript
- CSS Grids

## Thanks
I'm not going to pretend that I did this all by myself, the internet helped me.

Many, many thanks go to (in no particular order):  
- https://webpack.js.org/guides/  
- https://github.com/webpack/webpack/issues  
- https://github.com/JonathanMH/webpack-scss-sass-file  
- https://www.youtube.com/channel/UCSJbGtTlrDami-tDGPUV9-w  
- https://html5boilerplate.com/  